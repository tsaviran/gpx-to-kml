package main

import (
	"bufio"
	"fmt"
	"io"
	"time"

	"gitlab.com/tsaviran/xmlstream"
)

// kmlWriter is a streaming KML document writer.
type kmlWriter struct {
	w         *xmlstream.Writer
	doc       bool
	folder    bool
	track     bool
	line      bool
	placemark bool
	oldTS     *time.Time
}

// newKMLWriter creates new KML writer.
func newKMLWriter(io *io.Writer) kmlWriter {
	bio := bufio.NewWriter(*io)
	return kmlWriter{
		w:         xmlstream.NewWriter(bio),
		doc:       false,
		folder:    false,
		track:     false,
		line:      false,
		placemark: false,
	}
}

func (kml *kmlWriter) Finalise() {
	kml.maybeEndDocument()
}

// document

func (kml *kmlWriter) newDocument() {
	kml.w.StartElement("kml")
	kml.w.AddAttribute("xmlns", "http://www.opengis.net/kml/2.2")
	kml.w.AddAttribute("xmlns:gx", "http://www.google.com/kml/ext/2.2")
	kml.w.StartElement("Document")
	kml.w.OpenElement()
	kml.w.WriteRaw(`
	<Style id="normal">
		<IconStyle>
			<scale>0</scale>
			<hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>
		</IconStyle>
		<LineStyle>
			<color>ff0000ff</color>
			<width>3</width>
		</LineStyle>
	</Style>
	<Style id="highlight">
		<IconStyle>
			<scale>1.3</scale>
			<Icon>
				<href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>
			</Icon>
			<hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>
		</IconStyle>
		<LineStyle>
			<color>ff0000ff</color>
			<width>3</width>
		</LineStyle>
	</Style>
	<StyleMap id="style">
		<Pair>
			<key>normal</key>
			<styleUrl>#normal</styleUrl>
		</Pair>
		<Pair>
			<key>highlight</key>
			<styleUrl>#highlight</styleUrl>
		</Pair>
	</StyleMap>
`)
	kml.doc = true
}

func (kml *kmlWriter) maybeNewDocument() {
	if kml.doc {
		return
	}
	kml.newDocument()
}

func (kml *kmlWriter) endDocument() {
	kml.w.EndElement() // Document
	kml.doc = false
	kml.w.EndElement() // kml
}

func (kml *kmlWriter) maybeEndDocument() {
	kml.maybeEndFolder()
	if !kml.doc {
		return
	}
	kml.endDocument()
}

// folder

func (kml *kmlWriter) NewFolder(name *string) {
	kml.maybeNewDocument()
	kml.maybeEndFolder()

	kml.w.StartElement("Folder")
	if name != nil {
		kml.w.ElementCData("name", *name)
	}
	kml.folder = true
	kml.resetTS()
}

func (kml *kmlWriter) maybeNewFolder(name *string) {
	if kml.folder {
		return
	}
	kml.NewFolder(name)
}

func (kml *kmlWriter) endFolder() {
	kml.w.EndElement() // Folder
	kml.folder = false
}

func (kml *kmlWriter) maybeEndFolder() {
	kml.maybeEndPath()
	kml.maybeEndLine()
	if !kml.folder {
		return
	}
	kml.endFolder()
}

// placemark

func (kml *kmlWriter) newPlacemark(name *string) {
	kml.w.StartElement("Placemark")
	kml.w.ElementCData("styleUrl", "#style")
	if name != nil {
		kml.w.ElementCData("name", *name)
	}
	kml.placemark = true
}

func (kml *kmlWriter) maybeNewPlacemark(name *string) {
	if kml.placemark {
		return
	}
	kml.newPlacemark(name)
}

func (kml *kmlWriter) endPlacemark() {
	kml.w.EndElement() // Placemark
	kml.placemark = false
}

func (kml *kmlWriter) maybeEndPlacemark() {
	if !kml.placemark {
		return
	}
	kml.endPlacemark()
}

// track

func (kml *kmlWriter) newTrack(name *string) {
	kml.maybeEndPath()
	kml.maybeNewFolder(name)
	kml.newPlacemark(name)

	kml.w.StartElement("gx:Track")
	kml.track = true
}

func (kml *kmlWriter) endTrack() {
	kml.w.EndElement() // gx:Track
	kml.endPlacemark()
	kml.track = false
}

func (kml *kmlWriter) maybeEndTrack() {
	if !kml.track {
		return
	}
	kml.endTrack()
}

func (kml *kmlWriter) trackPoint(pt *point) {
	kml.maybeNewFolder(nil)
	kml.maybeNewPlacemark(nil)

	t := kml.getTS(pt)
	if t != nil {
		kml.w.ElementCData("when", t.Format(time.RFC3339))
	}

	var coord string
	if pt.elev != nil {
		coord = fmt.Sprintf("%f %f %f", pt.lon, pt.lat, *pt.elev)
	} else {
		coord = fmt.Sprintf("%f %f", pt.lon, pt.lat)
	}
	kml.w.ElementCData("gx:coord", coord)
}

// line

func (kml *kmlWriter) newLine(name *string) {
	kml.maybeEndPath()
	kml.maybeNewFolder(name)
	kml.newPlacemark(name)

	kml.w.StartElement("LineString")
	kml.w.ElementCData("extrude", "0")
	kml.w.ElementCData("tessellate", "0")
	kml.w.ElementCData("altitudeMode", "clampToGroup")
	kml.w.StartElement("coordinates")
	kml.line = true
}

func (kml *kmlWriter) endLine() {
	kml.w.EndElement() // coordinates
	kml.w.EndElement() // LineString
	kml.endPlacemark()
	kml.line = false
}

func (kml *kmlWriter) maybeEndLine() {
	if !kml.line {
		return
	}
	kml.endLine()
}

func (kml *kmlWriter) linePoint(pt *point) {
	if pt.elev != nil {
		kml.w.WriteCData(fmt.Sprintf("%.6f,%.6f,%.3f\n",
			pt.lon, pt.lat, *pt.elev))
	} else {
		kml.w.WriteCData(fmt.Sprintf("%.6f,%.6f\n",
			pt.lon, pt.lat))
	}
}

// path

func (kml *kmlWriter) inPath() bool {
	return kml.track || kml.line
}

func (kml *kmlWriter) maybeEndPath() {
	if kml.line {
		kml.endLine()
		return
	}
	if kml.track {
		kml.endTrack()
	}
}

func (kml *kmlWriter) resetTS() {
	kml.oldTS = nil
}

// point

func (kml *kmlWriter) newPoint(pt *point) {
	if !kml.inPath() {
		panic("not in path")
	}

	if kml.line {
		kml.linePoint(pt)
	} else {
		kml.trackPoint(pt)
	}
}

func (kml *kmlWriter) getTS(pt *point) *time.Time {
	if pt.time != nil {
		kml.oldTS = pt.time
		return pt.time
	} else if kml.oldTS != nil {
		return kml.oldTS
	}
	return nil
}
