package main

import (
	"log"
	"strconv"
	"time"

	"github.com/eliben/gosax"
)

type elementType int

// elements
const (
	MetaNameEl elementType = iota
	MetaTimeEl
	TrkNameEl
	PtEleEl
	PtTimeEl
	NoneEl
)

type point struct {
	lat   float64
	lon   float64
	elev  *float64
	time  *time.Time
	valid bool
}

func (pt *point) Clear() {
	pt.lat = 0.0
	pt.lon = 0.0
	pt.elev = nil
	pt.time = nil
	pt.valid = false
}

// gpxReader is a streaming GPX track reader.
type gpxReader struct {
	filename string

	ctxMeta bool // name, title
	ctxTrk  bool // name
	charCtx elementType

	wantEle         bool
	wantTime        bool
	wantEmptyTrk    bool
	wantEmptyTrkSeg bool

	trkSent bool
	trkName string

	trkSegSent bool

	pt point

	TrackCB   func(name string)
	SegmentCB func()
	PointCB   func(pt *point)
}

// newGPXReader creates new GPX reader.
func newGPXReader() gpxReader {
	reader := gpxReader{
		ctxMeta: false,
		ctxTrk:  false,
		charCtx: NoneEl,

		wantEle:  true,
		wantTime: true,

		wantEmptyTrk:    true,
		wantEmptyTrkSeg: true,

		pt: point{},
	}
	return reader
}

func (r *gpxReader) Read(filename string) error {
	cb := gosax.SaxCallbacks{
		StartElement: r.start,
		EndElement:   r.end,
		Characters:   r.chars,
	}
	// TODO wrap error
	return gosax.ParseFile(filename, cb)
}

func (r *gpxReader) start(name string, attrs []string) {
	if name == "metadata" {
		r.ctxMeta = true
	} else if name == "name" {
		if r.ctxMeta {
			r.charCtx = MetaNameEl
		} else if r.ctxTrk {
			r.charCtx = TrkNameEl
		} else {
			log.Print("unexpected name")
		}
	} else if name == "trk" {
		r.startTrk()
	} else if name == "trkseg" {
		r.startTrkSeg()
	} else if name == "trkpt" {
		r.startTrkPt(attrs)
	} else if name == "ele" {
		r.charCtx = PtEleEl
	} else if name == "time" {
		r.charCtx = PtTimeEl
	} else if name == "sat" {
		// pass
	} else {
		//log.Print("start", name)
	}
}

func (r *gpxReader) startTrk() {
	r.ctxMeta = false
	r.ctxTrk = true
	r.trkSent = false
	r.trkName = ""
}

func (r *gpxReader) startTrkSeg() {
	// log.Print("new segment")
	r.trkSegSent = false
	if r.wantEmptyTrkSeg {
		r.ensureTrkSeg() // implies ensureTrk
	} else if r.wantEmptyTrk {
		r.ensureTrk()
	}
}

func (r *gpxReader) startTrkPt(attrs []string) {
	var err error
	r.pt.Clear()
	gLat := false
	gLon := false
	for i := 0; i < len(attrs); i += 2 {
		if attrs[i] == "lat" {
			r.pt.lat, err = strconv.ParseFloat(attrs[i+1], 64)
			if err != nil {
				return
			}
			gLat = true
		} else if attrs[i] == "lon" {
			r.pt.lon, err = strconv.ParseFloat(attrs[i+1], 64)
			if err != nil {
				return
			}
			gLon = true
		}
		if gLat && gLon {
			r.pt.valid = true
			return
		}
	}
}

func (r *gpxReader) ensureTrk() {
	if r.trkSent {
		return
	}
	if r.TrackCB != nil {
		r.TrackCB(r.trkName)
	}
	r.trkSent = true
}

func (r *gpxReader) ensureTrkSeg() {
	r.ensureTrk()
	if r.trkSegSent {
		return
	}
	if r.SegmentCB != nil {
		r.SegmentCB()
	}
	r.trkSegSent = true
}

func (r *gpxReader) endTrkPt() {
	if !r.pt.valid {
		return
	}
	r.ensureTrkSeg() // implies ensureTrk

	if r.PointCB != nil {
		r.PointCB(&r.pt)
	}
}

func (r *gpxReader) end(name string) {
	r.charCtx = NoneEl
	if name == "metadata" {
		r.ctxMeta = false
	} else if name == "trkpt" {
		r.endTrkPt()
	}
}

func (r *gpxReader) chars(contents string) {
	if r.charCtx == NoneEl {
		return
	} else if r.charCtx == MetaNameEl {
	} else if r.charCtx == MetaTimeEl {
	} else if r.charCtx == TrkNameEl {
		r.trkName = contents
		if r.wantEmptyTrk {
			r.ensureTrk()
		}
	} else if r.charCtx == PtEleEl {
		r.ptEle(&contents)
	} else if r.charCtx == PtTimeEl {
		r.ptTime(&contents)
	} else {
		//log.Print("chars", contents)
	}
}

func (r *gpxReader) ptEle(ele *string) {
	if !r.wantEle {
		return
	}
	if !r.pt.valid {
		return
	}
	t, err := strconv.ParseFloat(*ele, 64)
	if err == nil {
		r.pt.elev = &t
	}
}

func (r *gpxReader) ptTime(ele *string) {
	if !r.wantTime {
		return
	}
	if !r.pt.valid {
		return
	}
	t, err := time.Parse(time.RFC3339, *ele)
	if err == nil {
		r.pt.time = &t
	}
}
