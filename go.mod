module gitlab.com/tsaviran/gpx-to-kml

go 1.15

require (
	github.com/StefanSchroeder/Golang-Ellipsoid v0.0.0-20200928074047-3758eb9e9574
	github.com/eliben/gosax v0.1.0
	gitlab.com/tsaviran/xmlstream v0.0.0-20191227000012-277e6ad57e2e
)
