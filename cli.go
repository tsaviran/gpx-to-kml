package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

type cli struct {
	tr *trans

	timeS float64
	distM float64
}

var outputMode = "auto"
var emptyTrack = false
var emptySegment = false
var listFiles = false
var printStats = false

func init() {
	flag.StringVar(&outputMode, "mode", "auto",
		"output mode, track or line")
	flag.BoolVar(&emptyTrack, "empty-track", false,
		"produce empty tracks")
	flag.BoolVar(&emptyTrack, "empty-segment", false,
		"produce empty segments")
	flag.BoolVar(&listFiles, "list-files", false,
		"list files as they are processed")
	flag.BoolVar(&printStats, "print-stats", false,
		"print statistics")
}

func main() {
	flag.Parse()

	w := bufio.NewWriter(os.Stdout)
	cli := cli{tr: newTrans(w)}
	defer cli.tr.Finalise()

	if outputMode == "auto" || outputMode == "track" {
		cli.tr.UseTracks()
	} else if outputMode == "line" {
		cli.tr.UseLines()
	}

	// TODO from stdin - limitation of github.com/eliben/gosax
	cli.transformFromArgs()

	if printStats {
		fmt.Fprintf(os.Stderr, "total distance: %.1f km\n",
			cli.distM/1000.0)
	}
}

func (cli *cli) transformFromArgs() {
	n := flag.NArg()
	if outputMode == "auto" {
		if n == 1 {
			cli.tr.UseTracks()
		} else {
			cli.tr.UseLines()
		}
	}

	for i, fn := range flag.Args() {
		cli.transformFile(fn, i, n)
	}
}

func (cli *cli) transformFile(fn string, i, n int) {
	if listFiles {
		fmt.Fprintf(os.Stderr, "%d/%d %s", i+1, n, fn)
	}
	distM, timeS := cli.tr.Transform(fn)
	if listFiles && printStats {
		distKM := distM / 1000.0
		fmt.Fprintf(os.Stderr, "  %.1f km", distKM)
		timeHr := timeS / 3600.0
		if timeS > 0.0 {
			fmt.Fprintf(os.Stderr, " / %.2f h", timeHr)
			fmt.Fprintf(os.Stderr, " / %.2f km/h", distKM/timeHr)
		}
	}
	if listFiles {
		fmt.Fprintf(os.Stderr, "\n")
	}
	cli.distM += distM
	cli.timeS += timeS
}
