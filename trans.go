package main

import (
	"io"
	"log"
	"time"

	"github.com/StefanSchroeder/Golang-Ellipsoid/ellipsoid"
)

type trans struct {
	dist float64

	kml       *kmlWriter
	r         gpxReader
	useTracks bool

	geo       ellipsoid.Ellipsoid
	prevTime  *time.Time
	firstTime *time.Time
	prevPt    point
}

// newTrans creates new GPX-to-KML transformer.
func newTrans(w io.Writer) *trans {
	kml := newKMLWriter(&w)

	t := trans{
		kml:       &kml,
		useTracks: true,
	}

	t.r = newGPXReader()
	t.r.TrackCB = t.newTrack
	t.r.PointCB = t.newPoint

	t.geo = ellipsoid.Init("WGS84", ellipsoid.Degrees, ellipsoid.Meter,
		ellipsoid.LongitudeIsSymmetric, ellipsoid.BearingIsSymmetric)

	return &t
}

func (t *trans) resetState() {
	t.dist = 0.0
	t.prevTime = nil
	t.firstTime = nil
	t.prevPt.valid = false
}

// UseLines sets output mode to KML lines.
func (t *trans) UseLines() {
	t.useTracks = false
}

// UseTracks sets output mode to KML tracks.
func (t *trans) UseTracks() {
	t.useTracks = true
}

// Finalise finalises output KML. Finalise must be called exactly once during
// the lifetime of trans or behaviour is undefined.
func (t *trans) Finalise() {
	t.kml.Finalise()
	t.kml = nil
}

// Transform translates GPX data in input filename to KML. It returns distance
// in metres and time in seconds.
func (t *trans) Transform(filename string) (float64, float64) {
	t.resetState()
	t.kml.NewFolder(&filename)
	err := t.r.Read(filename)
	if err != nil {
		log.Print(err)
	}

	var elapsed time.Duration
	if t.firstTime != nil && t.prevTime != nil {
		elapsed = t.prevTime.Sub(*t.firstTime)
	}

	return t.dist, elapsed.Seconds()
}

func (t *trans) newTrack(name string) {
	if t.useTracks {
		t.kml.newTrack(&name)
	} else {
		t.kml.newLine(&name)
	}
}

func (t *trans) newPoint(pt *point) {
	if t.prevPt.valid {
		d, _ := t.geo.To(t.prevPt.lat, t.prevPt.lon, pt.lat, pt.lon)
		t.dist += d
	}
	if t.firstTime == nil && pt.time != nil {
		t.firstTime = pt.time
	}
	if pt.time != nil {
		t.prevTime = pt.time
	}
	t.prevPt = *pt
	t.kml.newPoint(pt)
}
